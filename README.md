# Zarya Agence

## Our website

[`Link to our website`](https://agence-zarya.lpmiaw.univ-lr.fr)

## How to use

### Development

```bash
npm run dev
```

### Production

```bash
npm run build
```

## Us

- Colin LIENARD
- Bazile BINSON
- Manon BOUCHERON
- Gabriel GAUME