window.addEventListener('DOMContentLoaded', () => {
  if (window.innerWidth <= 1250) {
    const btn = document.querySelector('.navbar__burger');
    const menu = document.querySelector('.menu');
  
    btn.addEventListener('click', () =>{
      btn.classList.toggle('navbar__burger--active');
      menu.classList.toggle('menu--open');
    });
  }
});