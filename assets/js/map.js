var mymap = L.map('map').setView([46.1475, -1.156494], 13);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'sk.eyJ1IjoiZ2FiMmsiLCJhIjoiY2t3M3JnZ2k4Mjd2MzJ1cWkzcTZzZW05bCJ9.bjl6kw5_Q2dcHWL9iY4b_Q'
}).addTo(mymap);

var marker = L.marker([46.1475, -1.156494]).addTo(mymap);
marker.bindPopup("<b>Salut !</b><br>Nous sommes ici.").openPopup();