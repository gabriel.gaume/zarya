document.querySelectorAll('.card__link').forEach((link) => {
  link.addEventListener('click', (event) => {
    event.preventDefault();
    console.log(event.target.dataset.blank);
    document.querySelector(`#gauge-${event.target.dataset.gauge}`).classList.add('full');
    setTimeout(() => {
      if (event.target.dataset.blank) {
        window.open(event.target.href);
        document.querySelector(`#gauge-${event.target.dataset.gauge}`).classList.remove('full');
      } else {
        window.location.href = event.target.href;
      }
    }, 500);
  });
});

VanillaTilt.init([
  ...document.querySelectorAll('.card'),
  ...document.querySelectorAll('.menu__item'),
  ...document.querySelectorAll('.membre'),
], {
  glare: true,
  "max-glare": .2,
});
